#
# Cookbook Name:: WebPool
# Recipe:: nginx
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


# get implementation plan
plan_id = node['implementation_plan_id']
plan = search("implementation_plans","id:#{plan_id}").first

if plan == nil then
puts "Implementation Plan with id: #{plan_id} is not present in Implementation plans data bag"
exit
end

# Download nginx package

cookbook_file  "nginx_php.tar.gz" do
   path "/tmp/nginx_php.tar.gz"
   action :create
end

# unzip tar file

bash "install_Nginx-php" do
        user "root"
        cwd  "/opt"
        code <<-EOH
        tar -zxvf /tmp/nginx_php.tar.gz
	rm /tmp/nginx_php.tar.gz
        EOH
        not_if { ::File.exists?("/opt/nginx_installed") }
end

# Edit php  configuration file

template "/opt/php/lib64/php.ini" do
        action :create
        source "php.ini.erb"
	variables(
        :plan => plan
        )
end


# Download  memcached package
 
cookbook_file  "memcached.tar.gz" do
   path "/tmp/memcached.tar.gz"
   action :create
end

# unzip tar file

bash "install_memcached" do
        user "root"
        cwd  "/opt"
        code <<-EOH
        tar -zxvf /tmp/memcached.tar.gz
        rm /tmp/memcached.tar.gz
 	EOH
        not_if { ::File.exists?("/opt/nginx_installed") }
end


# Edit hosts file

template "/etc/hosts" do
        action :create
        source "hosts.erb"
	variables(
        :plan => plan
        )
end


# Install Web Application

cookbook_file  "index.php.nginx" do
   path "/opt/nginx/html/index.php"
   action :create
end


# Execute programms

bash "execute_programm" do
user "root"
environment 'LD_LIBRARY_PATH' => "/opt/memcached_libs/libevent/lib64/"
code <<-EOF

    FILE="/opt/nginx_installed"

    if [ ! -f $FILE ]; # if not installed
    then

    #Create user nginx and group nginx:
             groupadd nginx
             useradd nginx
             usermod -a -G nginx nginx

    #Create user www-data and group www-data:
            groupadd www-data
            useradd www-data
            usermod -a -G www-data www-data
            
            touch /opt/nginx_installed

    fi

    #path to pgrep command
    PGREP="/usr/bin/pgrep"
    HTTPD="nginx"
    # find nginx pid
    $PGREP ${HTTPD}
    if [ $? -ne 0 ] # if nginx  not running
    then

    #To start nginx:

       /opt/nginx/sbin/nginx

    #To start PHP-FPM:

       /opt/php/init.d.php-fpm start

   else

   #To restart nginx
         /opt/nginx/sbin/nginx -s reload
  
   #To restart PHP-FPM:

        /opt/php/init.d.php-fpm restart
 
   fi   

   # to make the socket readable (so that php works properly)

	chmod 777 /opt/php/php5-fpm.sock

   #To start Memcached:

       /opt/memcached/bin/memcached -u root -l 0.0.0.0 -p 11211 -M -m 64 -d

  
EOF
end

