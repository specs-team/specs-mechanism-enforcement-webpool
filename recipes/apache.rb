#
# Cookbook Name:: WebPool
# Recipe:: apache
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# get implementation plan
plan_id = node['implementation_plan_id']
plan = search("implementation_plans","id:#{plan_id}").first

if plan == nil then 
puts "Implementation Plan with id: #{plan_id} is not present in Implementation plans data bag"
exit
end

# Download package Apache-php-memcached

cookbook_file  "apache2-php5-memcached.tar.gz" do
   path "/tmp/apache2-php5-memcached.tar.gz"
   action :create
end

# unzip tar file

bash "install_program" do
        user "root"
        cwd  "/opt"
        code <<-EOH
        tar -zxvf /tmp/apache2-php5-memcached.tar.gz
        rm /tmp/apache2-php5-memcached.tar.gz
	EOH
        not_if { ::File.exists?("/opt/apache2") }
end
 
# Edit apache2-php5-memcached  configuration file
 
template "/opt/php5/etc/php.ini" do
        action :create
        source "php.ini.erb"
	variables(
	:plan => plan
	)
end

template "/etc/hosts" do
        action :create
        source "hosts.erb"
	variables(
	:plan => plan
	)
end

# Install Web Application

cookbook_file  "index.php.apache" do
   path "/opt/apache2/htdocs/index.php"
   action :create
end
 

# Run Apache2-php5-memcached service

bash "execute_programm" do
        user "root"
	cwd "/opt"
        environment 'LD_LIBRARY_PATH' => "/opt/dependencies/memcache/libevent/lib64/"
        code <<-EOH
       
        # enable memcached
	/opt/memcached/bin/memcached -u root -l 0.0.0.0 -p 11211 -M -m 64 -d	

	#path to pgrep command
	PGREP="/usr/bin/pgrep"
	HTTPD="httpd"
	# find httpd pid
	$PGREP ${HTTPD}
	if [ $? -ne 0 ] # if apache not running
	then
   		/opt/apache2/bin/apachectl start
	else
		/opt/apache2/bin/apachectl restart
	fi
        EOH
	end



