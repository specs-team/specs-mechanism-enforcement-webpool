#
# Cookbook Name:: WebPool
# Recipe:: haproxy
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute


# get implementation plan
plan_id = node['implementation_plan_id']
plan = search("implementation_plans","id:#{plan_id}").first

if plan == nil then
puts "Implementation Plan with id: #{plan_id} is not present in Implementation plans data bag"
exit
end

# Download package HAProxy

cookbook_file  "haproxy-hatop.tar.gz" do
   path "/tmp/haproxy-hatop.tar.gz"
   action :create
end

# unzip tar file

bash "install_program" do
	user "root"
        cwd  "/opt"
	code <<-EOH
        tar -zxvf /tmp/haproxy-hatop.tar.gz
	rm /tmp/haproxy-hatop.tar.gz
	EOH
	not_if { ::File.exists?("/opt/haproxy") }
end

# Edit haproxy configuration file

template "/opt/haproxy/configs/haproxy.cfg" do
	action :create
	source "haproxy_config.erb"
	variables(
        :plan => plan
        )
end

# Map Hostname - IP address

template "/etc/hosts" do
        action :create
        source "hosts.erb"
	variables(
        :plan => plan
        )
end

# Run HAProxy service

bash "execute_programm" do
        user "root"
        code <<-EOH
        rm -f /opt/haproxy/haproxy_sock
 	/opt/haproxy-1.5.9/haproxy -f /opt/haproxy/configs/haproxy.cfg
	EOH
end


       
